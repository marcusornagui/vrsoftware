package gui;

import static classe.MatriculaAlterar.matriculaAlterarConsultar;
import static classe.MatriculaAlterar.matriculaAlterarSalvar;
import classe.MatriculaIncluir;
import static classe.MatriculaIncluir.matriculaPesquisaAluno;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JOptionPane;

public class MatriculaAlterarGUI extends javax.swing.JDialog {

    int codMatricula;

    public MatriculaAlterarGUI(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    public MatriculaAlterarGUI(java.awt.Frame parent, boolean modal, int codMatricula) {
        super(parent, modal);
        initComponents();

        //CONFIRMAÇÃO DE SAÍDA
        setDefaultCloseOperation(this.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                confirmaSaida();
            }
        });

        this.codMatricula = codMatricula;
        consultaMatricula();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        txtNomeAluno = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtCodigoAluno = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        txtCodigoCurso = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        txtDescricaoCurso = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtCodigoMatricula = new javax.swing.JTextField();
        btnSalvar = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        btnSair = new javax.swing.JButton();
        btnSalvarGui = new javax.swing.JButton();

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Incluir Aluno");
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtNomeAluno.setEditable(false);
        jPanel2.add(txtNomeAluno, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 80, 420, -1));

        jLabel4.setText("Aluno");
        jPanel2.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, -1, -1));

        txtCodigoAluno.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCodigoAlunoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCodigoAlunoKeyReleased(evt);
            }
        });
        jPanel2.add(txtCodigoAluno, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, 84, -1));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/consultar.png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 80, 22, 22));

        jLabel6.setText("Curso");
        jPanel2.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, -1, -1));

        txtCodigoCurso.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCodigoCursoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCodigoCursoKeyReleased(evt);
            }
        });
        jPanel2.add(txtCodigoCurso, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 130, 84, -1));

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/consultar.png"))); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 130, 22, 22));

        txtDescricaoCurso.setEditable(false);
        jPanel2.add(txtDescricaoCurso, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 130, 420, -1));

        jLabel5.setText("Código");
        jPanel2.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        txtCodigoMatricula.setEditable(false);
        jPanel2.add(txtCodigoMatricula, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 84, -1));

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 37, 560, 170));

        btnSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/salvar.png"))); // NOI18N
        btnSalvar.setToolTipText("Salvar");
        btnSalvar.setBorder(null);
        btnSalvar.setContentAreaFilled(false);
        btnSalvar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });
        jPanel1.add(btnSalvar, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 11, 20, 20));

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnSair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/sair.png"))); // NOI18N
        btnSair.setText("Sair");
        btnSair.setToolTipText("Sair");
        btnSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSairActionPerformed(evt);
            }
        });
        jPanel4.add(btnSair, new org.netbeans.lib.awtextra.AbsoluteConstraints(482, 5, -1, 26));

        btnSalvarGui.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/salvar.png"))); // NOI18N
        btnSalvarGui.setText("Salvar");
        btnSalvarGui.setToolTipText("Salvar");
        btnSalvarGui.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarGuiActionPerformed(evt);
            }
        });
        jPanel4.add(btnSalvarGui, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 5, -1, 26));

        jPanel1.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 220, 560, 35));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 580, 270));

        setSize(new java.awt.Dimension(601, 311));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        alterarMatricula();
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void btnSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSairActionPerformed
        confirmaSaida();
    }//GEN-LAST:event_btnSairActionPerformed

    private void btnSalvarGuiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarGuiActionPerformed
        alterarMatricula();
    }//GEN-LAST:event_btnSalvarGuiActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        new AlunoConsultarGUI(null, rootPaneCheckingEnabled, txtCodigoAluno, txtNomeAluno).setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        new CursoConsultarGUI(null, rootPaneCheckingEnabled, txtCodigoCurso, txtDescricaoCurso).setVisible(true);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void txtCodigoAlunoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCodigoAlunoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            matriculaPesquisaAluno(txtCodigoAluno, txtNomeAluno);
            txtCodigoCurso.requestFocus();
        }
    }//GEN-LAST:event_txtCodigoAlunoKeyPressed

    private void txtCodigoCursoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCodigoCursoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            MatriculaIncluir.matriculaPesquisaCurso(txtCodigoCurso, txtDescricaoCurso);
            btnSalvarGui.requestFocus();
        }
    }//GEN-LAST:event_txtCodigoCursoKeyPressed

    private void txtCodigoAlunoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCodigoAlunoKeyReleased
        //REMOVE LETRAS E CARACTERES ESPECIAIS
        txtCodigoAluno.setText(txtCodigoAluno.getText().replaceAll("[^0-9]", "")); 
    }//GEN-LAST:event_txtCodigoAlunoKeyReleased

    private void txtCodigoCursoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCodigoCursoKeyReleased
        //REMOVE LETRAS E CARACTERES ESPECIAIS
        txtCodigoCurso.setText(txtCodigoCurso.getText().replaceAll("[^0-9]", "")); 
    }//GEN-LAST:event_txtCodigoCursoKeyReleased

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MatriculaAlterarGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MatriculaAlterarGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MatriculaAlterarGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MatriculaAlterarGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                MatriculaAlterarGUI dialog = new MatriculaAlterarGUI(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSair;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JButton btnSalvarGui;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JTextField txtCodigoAluno;
    private javax.swing.JTextField txtCodigoCurso;
    private javax.swing.JTextField txtCodigoMatricula;
    private javax.swing.JTextField txtDescricaoCurso;
    private javax.swing.JTextField txtNomeAluno;
    // End of variables declaration//GEN-END:variables

    //CHAMA FUNÇÃO PARA ALTERAR MATRÍCULA
    void alterarMatricula() {
        if (!txtCodigoAluno.getText().equals("") && !txtCodigoCurso.getText().equals("")) {
            matriculaAlterarSalvar(codMatricula, Integer.parseInt(txtCodigoAluno.getText()), Integer.parseInt(txtCodigoCurso.getText()));
            this.dispose();
        } else {
            JOptionPane.showMessageDialog(null, "Há campo(s) obrigatório(s) em branco.");
        }
    }

    //CHAMA FUNÇÃO PARA CONSULTAR INFORMAÇÕES DO MATRÍCULA
    void consultaMatricula() {
        matriculaAlterarConsultar(codMatricula, txtCodigoMatricula, txtCodigoAluno, txtCodigoCurso);
        matriculaPesquisaAluno(txtCodigoAluno, txtNomeAluno);
        MatriculaIncluir.matriculaPesquisaCurso(txtCodigoCurso, txtDescricaoCurso);
    }

    //CONFIRMAR SALVAMENTO DE INFORMAÇÕES AO FECHAR A TELA
    void confirmaSaida() {
        if (!txtNomeAluno.getText().equals("")) {
            int confirmaSaida = JOptionPane.showConfirmDialog(this, "Deseja salvar as alterações?");
            if (confirmaSaida == 0) {
                alterarMatricula();
            }
            if (confirmaSaida == 1) {
                this.dispose();
            }
        } else {
            this.dispose();
        }
    }
}
