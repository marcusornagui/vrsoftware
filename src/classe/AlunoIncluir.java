package classe;

import dao.ConectaBanco;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class AlunoIncluir {

    //INSERE ALUNO NO BANCO DE DADOS
    public static void alunoIncluir(String nomeAluno) {
        Connection con = new ConectaBanco().conectarBanco();
        PreparedStatement stmt = null;
        try {
            StringBuilder sql = null;
            sql = new StringBuilder();
            sql.append("INSERT INTO aluno (nome) VALUES (");
            sql.append("'" + nomeAluno + "');");

            stmt = con.prepareStatement(sql.toString());
            stmt.executeUpdate();

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao incluir aluno.\n" + ex);
        } finally {
            ConectaBanco.fechaConexao(con);
            JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
        }
    }
}
