package classe;

import dao.ConectaBanco;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class AlunoAlterar {

    //CONSULTA CADASTRO DO ALUNO NO BANCO DE DADOS PARA MOSTRAR NA GUI
    public static void alunoAlterarConsultar(int codAluno, JTextField txtCodigoAluno, JTextField txtNomeAluno) {
        Connection con = new ConectaBanco().conectarBanco();
        PreparedStatement stmt = null;
        try {
            StringBuilder sql = null;
            sql = new StringBuilder();
            sql.append("SELECT codigo, nome FROM aluno");
            sql.append(" WHERE codigo = ");
            sql.append(codAluno);

            stmt = con.prepareStatement(sql.toString());
            ResultSet resultado = stmt.executeQuery();

            if (resultado.next()) {
                resultado.first();
                txtCodigoAluno.setText(resultado.getString(1));
                txtNomeAluno.setText(resultado.getString(2));
            } else {
                JOptionPane.showMessageDialog(null, "Registro(s) não encontrado(s)!");
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao consultar aluno.\n" + ex);
        }
    }

    //ALTERA NO BANCO DE DADOS O CADASTRO DO ALUNO
    public static void alunoAlterarSalvar(int codAluno, String nomeAluno) {
        Connection con = new ConectaBanco().conectarBanco();
        PreparedStatement stmt = null;
        try {
            StringBuilder sql = null;
            sql = new StringBuilder();
            sql.append("UPDATE aluno SET nome =");
            sql.append(" '" + nomeAluno + "'");
            sql.append(" WHERE codigo = ");
            sql.append(codAluno);

            stmt = con.prepareStatement(sql.toString());
            stmt.executeUpdate();

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao alterar aluno.\n" + ex);
        } finally {
            ConectaBanco.fechaConexao(con);
            JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
        }
    }
}
