package classe;

import dao.ConectaBanco;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

//CRIA BANCO DE DADOS
public class CriarBancoDados {
    
    public static void criarBancoDados() {
        Connection con = new ConectaBanco().conectarBancoCriarBanco();
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("CREATE DATABASE IF NOT EXISTS bancovr");
            int existeBanco = stmt.executeUpdate();
            //VERIFICA SE O BANCO FOI CRIADO, CASO FOR, ELE CRIA AS TABELAS
            if (existeBanco != 0) {
                criarCurso();
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao criar Banco de Dados" + ex);
        } finally {
            ConectaBanco.fechaConexao(con);
        }
    }

    //CRIA TABELA curso
    private static void criarCurso() {
        Connection con = new ConectaBanco().conectarBancoCriarBanco();
        PreparedStatement stmt = null;

        StringBuilder sql = null;
        sql = new StringBuilder();
        sql.append("CREATE TABLE bancovr.curso (");
        sql.append(" codigo int(11) NOT NULL AUTO_INCREMENT,");
        sql.append(" descricao varchar(50) NOT NULL,");
        sql.append(" ementa text NOT NULL,");
        sql.append(" PRIMARY KEY (codigo)");
        sql.append(") ENGINE=InnoDB DEFAULT CHARSET=latin1;");

        try {
            stmt = con.prepareStatement(sql.toString());
            stmt.executeUpdate();
            criarAluno();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao criar tabela Curso" + ex);
        } finally {
            ConectaBanco.fechaConexao(con);
        }
    }

    //CRIA TABELA aluno
    private static void criarAluno() {
        Connection con = new ConectaBanco().conectarBancoCriarBanco();
        PreparedStatement stmt = null;

        StringBuilder sql = null;
        sql = new StringBuilder();
        sql.append("CREATE TABLE bancovr.aluno (");
        sql.append(" codigo int(11) NOT NULL AUTO_INCREMENT,");
        sql.append(" nome varchar(50) NOT NULL,");
        sql.append(" PRIMARY KEY (codigo)");
        sql.append(") ENGINE=InnoDB DEFAULT CHARSET=latin1;");

        try {
            stmt = con.prepareStatement(sql.toString());
            stmt.executeUpdate();
            criarCursoAluno();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao criar tabela Curso" + ex);
        } finally {
            ConectaBanco.fechaConexao(con);
        }
    }

    //CRIA TABELA curso_aluno
    private static void criarCursoAluno() {
        Connection con = new ConectaBanco().conectarBancoCriarBanco();
        PreparedStatement stmt = null;

        StringBuilder sql = null;
        sql = new StringBuilder();
        sql.append("CREATE TABLE bancovr.curso_aluno (");
        sql.append(" codigo int(11) NOT NULL AUTO_INCREMENT,");
        sql.append(" codigo_aluno int(11) NOT NULL,");
        sql.append(" codigo_curso int(11) NOT NULL,");
        sql.append(" PRIMARY KEY (codigo)");
        sql.append(") ENGINE=InnoDB DEFAULT CHARSET=latin1;");

        try {
            stmt = con.prepareStatement(sql.toString());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao criar tabela Curso" + ex);
        }

        //CRIA CHAVE ESTRANGEIRA fk_aluno
        sql = new StringBuilder();
        sql.append("ALTER TABLE bancovr.curso_aluno ADD CONSTRAINT fk_aluno FOREIGN KEY (codigo_aluno) REFERENCES bancovr.aluno (codigo);");

        try {
            stmt = con.prepareStatement(sql.toString());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao criar tabela Curso" + ex);
        }

        //CRIA CHAVE ESTRANGEIRA fk_curso
        sql = new StringBuilder();
        sql.append("ALTER TABLE bancovr.curso_aluno ADD CONSTRAINT fk_curso FOREIGN KEY (codigo_curso) REFERENCES bancovr.curso (codigo);");

        try {
            stmt = con.prepareStatement(sql.toString());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao criar tabela Curso" + ex);
        } finally {
            ConectaBanco.fechaConexao(con);
        }
    }

}
