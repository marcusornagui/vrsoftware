package classe;

import dao.ConectaBanco;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class MatriculaConsultar {

    //BUSCA INFORMAÇÕE DAS MATRÍCULAS NO BANDO DE DADOS, E LISTA NA TABELA DA GUI
    public static void matriculaConsultar(JTable tblAlunos, String pesquisaCodigo, 
            String pesquisaAluno, String pesquisaCurso, JLabel labNumeroRegistros) {

        int numeroRegistros = 0;

        DefaultTableModel modeloTabela = (DefaultTableModel) tblAlunos.getModel();
        modeloTabela.setNumRows(0);

        Connection con = new ConectaBanco().conectarBanco();
        PreparedStatement stmt = null;
        try {
            StringBuilder sql = null;
            sql = new StringBuilder();
            sql.append("SELECT curso_aluno.codigo, aluno.nome, curso.descricao");
            sql.append(" FROM curso_aluno");
            sql.append(" LEFT JOIN aluno ON aluno.codigo = curso_aluno.codigo_aluno");
            sql.append(" LEFT JOIN curso ON curso.codigo = curso_aluno.codigo_curso");
            sql.append(" WHERE 1 = 1");

            if (!pesquisaCodigo.equals("")) {
                sql.append(" AND curso_aluno.codigo = " + pesquisaCodigo);
            }
            if (!pesquisaAluno.equals("")) {
                sql.append(" AND aluno.nome LIKE '%" + pesquisaAluno + "%'");
            }
            if (!pesquisaCurso.equals("")) {
                sql.append(" AND curso.descricao LIKE '%" + pesquisaCurso + "%'");
            }

            stmt = con.prepareStatement(sql.toString());
            ResultSet resultado = stmt.executeQuery();

            while (resultado.next()) {
                modeloTabela.addRow(new Object[]{resultado.getString(1), resultado.getString(2), resultado.getString(3)});
                numeroRegistros++;
            }
            if (numeroRegistros == 0) {
                JOptionPane.showMessageDialog(null, "Registro(s) não encontrado(s)!");
            }
            
            labNumeroRegistros.setText(numeroRegistros + " Registro(s) encontrado(s)");

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao buscar matriculas" + ex);
        } finally {
            ConectaBanco.fechaConexao(con);
        }
    }

    //EXCLUI CURSO DO BANCO DE DADOS
    public static void matriculaExcluir(int codMatricula) {
        Connection con = new ConectaBanco().conectarBanco();
        PreparedStatement stmt = null;
        try {
            StringBuilder sql = null;
            sql = new StringBuilder();
            sql.append("DELETE FROM curso_aluno ");
            sql.append(" WHERE codigo = ");
            sql.append(codMatricula);

            stmt = con.prepareStatement(sql.toString());
            stmt.executeUpdate();

            JOptionPane.showMessageDialog(null, "Excluído com sucesso!");

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao excluir Matrícula.\n" + ex);
        } finally {
            ConectaBanco.fechaConexao(con);
        }
    }
}
