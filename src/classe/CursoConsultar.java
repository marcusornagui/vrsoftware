package classe;

import dao.ConectaBanco;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class CursoConsultar {

    //BUSCA INFORMAÇÕE DOS CURSO NO BANDO DE DADOS, E LISTA NA TABELA DA GUI
    public static void cursoConsultar(JTable tblAlunos, String pesquisaCodigo, 
            String pesquisaNome, String pesquisaEmenta, JLabel labNumeroRegistros) {

        int numeroRegistros = 0;

        DefaultTableModel modeloTabela = (DefaultTableModel) tblAlunos.getModel();
        modeloTabela.setNumRows(0);

        Connection con = new ConectaBanco().conectarBanco();
        PreparedStatement stmt = null;
        try {
            StringBuilder sql = null;
            sql = new StringBuilder();
            sql.append("SELECT codigo, descricao, ementa FROM curso");
            sql.append(" WHERE 1 = 1");

            if (!pesquisaCodigo.equals("")) {
                sql.append(" AND codigo = " + pesquisaCodigo);
            }
            if (!pesquisaNome.equals("")) {
                sql.append(" AND descricao LIKE '%" + pesquisaNome + "%'");
            }
            if (!pesquisaEmenta.equals("")) {
                sql.append(" AND ementa LIKE '%" + pesquisaEmenta + "%'");
            }

            stmt = con.prepareStatement(sql.toString());
            ResultSet resultado = stmt.executeQuery();

            while (resultado.next()) {
                modeloTabela.addRow(new Object[]{resultado.getString(1), resultado.getString(2)});
                numeroRegistros++;
            }
            if (numeroRegistros == 0) {
                JOptionPane.showMessageDialog(null, "Registro(s) não encontrado(s)!");
            }

            labNumeroRegistros.setText(numeroRegistros + " Registro(s) encontrado(s)");
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao buscar Cursos" + ex);
        } finally {
            ConectaBanco.fechaConexao(con);
        }
    }

    //EXCLUI CURSO DO BANCO DE DADOS
    public static void cursoExcluir(int codCurso) {
        Connection con = new ConectaBanco().conectarBanco();
        PreparedStatement stmt = null;
        try {
            StringBuilder sql = null;
            sql = new StringBuilder();
            sql.append("DELETE FROM curso ");
            sql.append(" WHERE codigo = ");
            sql.append(codCurso);

            stmt = con.prepareStatement(sql.toString());
            stmt.executeUpdate();

            JOptionPane.showMessageDialog(null, "Excluído com sucesso!");

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao excluir Curso.\nVerifique se o Curso não esta sendo utilizado em uma matrícula");
        } finally {
            ConectaBanco.fechaConexao(con);
        }
    }
}
