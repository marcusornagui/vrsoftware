package classe;

import dao.ConectaBanco;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class MatriculaIncluir {

    //INSERE MATRÍCULA NO BANCO DE DADOS
    public static void matriculaIncluir(int codAluno, int codCurso) {
        Connection con = new ConectaBanco().conectarBanco();
        PreparedStatement stmt = null;
        try {
            StringBuilder sql = null;
            sql = new StringBuilder();
            sql.append("INSERT INTO curso_aluno (codigo_aluno, codigo_curso) VALUES (");
            sql.append(codAluno + ",");
            sql.append(codCurso + ");");

            stmt = con.prepareStatement(sql.toString());
            stmt.executeUpdate();

            JOptionPane.showMessageDialog(null, "Salvo com sucesso!");

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao incluir Matricula.\nVerifique cadastro de Aluno e Curso.");
        } finally {
            ConectaBanco.fechaConexao(con);
        }

    }

    //PESQUISA INFORMAÇÕES DO ALUNO CONFORME CÓDIGO
    public static void matriculaPesquisaAluno(JTextField txtCodigoAluno, JTextField txtNomeAluno) {
        Connection con = new ConectaBanco().conectarBanco();
        PreparedStatement stmt = null;
        try {
            StringBuilder sql = null;
            sql = new StringBuilder();
            sql.append("SELECT codigo, nome FROM aluno");
            sql.append(" WHERE codigo = ");
            sql.append(txtCodigoAluno.getText());

            stmt = con.prepareStatement(sql.toString());
            ResultSet resultado = stmt.executeQuery();

            if (resultado.next()) {
                resultado.first();
                txtCodigoAluno.setText(resultado.getString(1));
                txtNomeAluno.setText(resultado.getString(2));
            } else {
                JOptionPane.showMessageDialog(null, "Registro(s) não encontrado(s)!");
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao consultar matrícula.\n" + ex);
        }
    }

    //PESQUISA INFORMAÇÕES DO ALUNO CONFORME CÓDIGO
    public static void matriculaPesquisaCurso(JTextField txtCodigoCurso, JTextField txtDescricaoCurso) {
        Connection con = new ConectaBanco().conectarBanco();
        PreparedStatement stmt = null;
        try {
            StringBuilder sql = null;
            sql = new StringBuilder();
            sql.append("SELECT codigo, descricao FROM curso");
            sql.append(" WHERE codigo = ");
            sql.append(txtCodigoCurso.getText());

            stmt = con.prepareStatement(sql.toString());
            ResultSet resultado = stmt.executeQuery();

            if (resultado.next()) {
                resultado.first();
                txtCodigoCurso.setText(resultado.getString(1));
                txtDescricaoCurso.setText(resultado.getString(2));
            } else {
                JOptionPane.showMessageDialog(null, "Registro(s) não encontrado(s)!");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao consultar aluno.\n" + ex);
        }
    }

}
