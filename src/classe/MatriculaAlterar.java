package classe;

import dao.ConectaBanco;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class MatriculaAlterar {

    //CONSULTA CADASTRO DA MATRÍCULA NO BANCO DE DADOS PARA MOSTRAR NA GUI
    public static void matriculaAlterarConsultar(int codMatricula, JTextField txtCodigoMatricula, JTextField txtCodigoAluno, JTextField txtCodigoCurso) {
        Connection con = new ConectaBanco().conectarBanco();
        PreparedStatement stmt = null;
        try {
            StringBuilder sql = null;
            sql = new StringBuilder();
            sql.append("SELECT codigo, codigo_aluno, codigo_curso FROM curso_aluno");
            sql.append(" WHERE codigo = ");
            sql.append(codMatricula);

            stmt = con.prepareStatement(sql.toString());
            ResultSet resultado = stmt.executeQuery();

            if (resultado.next()) {
                resultado.first();
                txtCodigoMatricula.setText(resultado.getString(1));
                txtCodigoAluno.setText(resultado.getString(2));
                txtCodigoCurso.setText(resultado.getString(3));
            } else {
                JOptionPane.showMessageDialog(null, "Registro(s) não encontrado(s)!");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao consultar matricula.\n" + ex);
        }
    }

    //ALTERA INFORMAÇÕES DA MATRÍCULA NO BANCO DE DADOS
    public static void matriculaAlterarSalvar(int codMatricula, int codAluno, int codCurso) {
        Connection con = new ConectaBanco().conectarBanco();
        PreparedStatement stmt = null;
        try {
            StringBuilder sql = null;
            sql = new StringBuilder();
            sql.append("UPDATE curso_aluno SET ");
            sql.append(" codigo_aluno = " + codAluno + ",");
            sql.append(" codigo_curso = " + codCurso);
            sql.append(" WHERE codigo = ");
            sql.append(codMatricula);

            stmt = con.prepareStatement(sql.toString());
            stmt.executeUpdate();

            JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao alterar Matrícula.\nVerifique cadastro de Aluno e Curso.");
        } finally {
            ConectaBanco.fechaConexao(con);
        }
    }
}
