package classe;

import dao.ConectaBanco;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class CursoIncluir {

    //INSERE CURSO NO BANCO DE DADOS
    public static void cursoIncluir(String descricaoCurso, String ementaCurso) {
        Connection con = new ConectaBanco().conectarBanco();
        PreparedStatement stmt = null;
        try {
            StringBuilder sql = null;
            sql = new StringBuilder();
            sql.append("INSERT INTO curso (descricao, ementa) VALUES (");
            sql.append("'" + descricaoCurso + "',");
            sql.append("'" + ementaCurso + "');");

            stmt = con.prepareStatement(sql.toString());
            stmt.executeUpdate();

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao incluir Curso.\n" + ex);
        } finally {
            ConectaBanco.fechaConexao(con);
            JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
        }
    }
}
