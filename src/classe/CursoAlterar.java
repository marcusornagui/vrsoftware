package classe;

import dao.ConectaBanco;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class CursoAlterar {

    //CONSULTA CADASTRO DO CURSO NO BANCO DE DADOS PARA MOSTRAR NA GUI
    public static void cursoAlterarConsultar(int codCurso, JTextField txtCodigoCurso, JTextField txtDescricaoCurso, JTextArea taEmenta) {
        Connection con = new ConectaBanco().conectarBanco();
        PreparedStatement stmt = null;
        try {
            StringBuilder sql = null;
            sql = new StringBuilder();
            sql.append("SELECT codigo, descricao, ementa FROM curso");
            sql.append(" WHERE codigo = ");
            sql.append(codCurso);

            stmt = con.prepareStatement(sql.toString());
            ResultSet resultado = stmt.executeQuery();

            if (resultado.next()) {
                resultado.first();
                txtCodigoCurso.setText(resultado.getString(1));
                txtDescricaoCurso.setText(resultado.getString(2));
                taEmenta.setText(resultado.getString(3));
            } else {
                JOptionPane.showMessageDialog(null, "Registro(s) não encontrado(s)!");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao consultar curso.\n" + ex);
        }
    }

    //ALTERA INFORMAÇÕES DO CURSO NO BANCO DE DADOS
    public static void cursoAlterarSalvar(int codCurso, String descricaoCurso, String ementaCurso) {
        Connection con = new ConectaBanco().conectarBanco();
        PreparedStatement stmt = null;
        try {
            StringBuilder sql = null;
            sql = new StringBuilder();
            sql.append("UPDATE curso SET ");
            sql.append(" descricao = '" + descricaoCurso + "',");
            sql.append(" ementa = '" + ementaCurso + "'");
            sql.append(" WHERE codigo = ");
            sql.append(codCurso);

            stmt = con.prepareStatement(sql.toString());
            stmt.executeUpdate();

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao alterar curso.\n" + ex);
        } finally {
            ConectaBanco.fechaConexao(con);
            JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
        }
    }
}
