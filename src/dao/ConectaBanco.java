package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class ConectaBanco {

    private static final String driver = "com.mysql.jdbc.Driver";
    private static final String URL = "jdbc:mysql://localhost:3306/";
    private static final String nomeBanco = "bancovr";
    private static final String usuarioBanco = "root";
    private static final String senhaBanco = "";
    public Statement s;

    //CONEXÃO PARA MANIPULAR BANCO DE DADOS
    public static Connection conectarBanco() {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao conectar ao Banco de Dados" + ex);
        }

        try {
            return DriverManager.getConnection(URL + nomeBanco, usuarioBanco, senhaBanco);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao conectar ao Banco de Dados" + ex);
        }
        return null;
    }
    
    //CONEXÃO PARA CRIAR BANCO DE DADOS
    public static Connection conectarBancoCriarBanco() {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao conectar ao Banco de Dados" + ex);
        }

        try {
            return DriverManager.getConnection(URL, usuarioBanco, senhaBanco);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao conectar ao Banco de Dados\n Verifique se o serviço do MySQL está ativo");
        }
        return null;
    }

    //FECHA CONEXÃO COM O BANCO DE DADOS
    public static void fechaConexao(Connection con) {
        if (con != null) {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(ConectaBanco.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
